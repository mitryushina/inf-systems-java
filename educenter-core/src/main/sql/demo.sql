set identity_insert [dbo].[COURSE_COMPLEXITIES] on

insert into [dbo].[COURSE_COMPLEXITIES] ([id], [title])
  values (1, 'Basic');

insert into [dbo].[COURSE_COMPLEXITIES] ([id], [title])
  values (2, 'Medium');

insert into [dbo].[COURSE_COMPLEXITIES] ([id], [title])
  values (3, 'Complex');

insert into [dbo].[COURSE_COMPLEXITIES] ([id], [title])
  values (4, 'Professional');

set identity_insert [dbo].[COURSE_COMPLEXITIES] OFF
go

set identity_insert [dbo].[COURSES] on

insert into [dbo].[COURSES] ([id], [code], [title], [complexity_id])
  values (1, 'ALG', 'Algorithms', 2);
insert into [dbo].[COURSES] ([id], [code], [title], [complexity_id])
  values (2, 'OOP', 'Object-Oriented Programming', 2);
insert into [dbo].[COURSES] ([id], [code], [title], [complexity_id])
  values (3, 'ML', 'Machine Learning', 3);
insert into [dbo].[COURSES] ([id], [code], [title], [complexity_id])
  values (4, 'PROG', 'Programming Basics', 1);

set identity_insert [dbo].[COURSES] off

set identity_insert [dbo].[CONTACTS] on

insert into [dbo].[CONTACTS] ([id], [address], [phone_number], [email])
  values (1, 'ul. Baumanskaya 2-ya, 5, Moscow', '+1-202-555-0189', 'user@example.com');
insert into [dbo].[CONTACTS] ([id], [address], [phone_number], [email])
  values (2, 'ul. Baumanskaya 2-ya, 5, Moscow', '+1-202-555-0101', 'user@example.com');
insert into [dbo].[CONTACTS] ([id], [address], [phone_number], [email])
  values (3, 'ul. Baumanskaya 2-ya, 5, Moscow', '+1-202-555-0158', 'user@example.com');
insert into [dbo].[CONTACTS] ([id], [address], [phone_number], [email])
  values (4, 'ul. Baumanskaya 2-ya, 5, Moscow', '+1-202-555-0147', 'user@example.com');
insert into [dbo].[CONTACTS] ([id], [address], [phone_number], [email])
  values (5, 'ul. Baumanskaya 2-ya, 5, Moscow', '+1-202-555-0167', 'user@example.com');
insert into [dbo].[CONTACTS] ([id], [address], [phone_number], [email])
  values (6, 'ul. Baumanskaya 2-ya, 5, Moscow', '+1-202-555-0170', 'user@example.com');

set identity_insert [dbo].[CONTACTS] off

set identity_insert [dbo].[PATIENTS] on

insert into [dbo].[PATIENTS] ([id], [full_name], [birth_date], [contact_id])
  values (1, 'Ivanov Ivan Ivanovich', '1991-04-05', 1);
insert into [dbo].[PATIENTS] ([id], [full_name], [birth_date], [contact_id])
  values (2, 'Sidorov Denis Petrovich', '1965-03-05', 2);
insert into [dbo].[PATIENTS] ([id], [full_name], [birth_date], [contact_id])
  values (3, 'Petrov Denis Semenovich', '1987-05-09', 3);
insert into [dbo].[PATIENTS] ([id], [full_name], [birth_date], [contact_id])
  values (4, 'Aksjutina Katerina Mihailovna', '1905-04-05', 4);
insert into [dbo].[PATIENTS] ([id], [full_name], [birth_date], [contact_id])
  values (5, 'Mitryushina Natalya Nicolaevna', '1997-03-05', 5);
insert into [dbo].[PATIENTS] ([id], [full_name], [birth_date], [contact_id])
  values (6, 'Petrov Denis Semenovich', '1987-05-09', 6);

set identity_insert [dbo].[PATIENTS] off

set identity_insert [dbo].[RECEPTIONS] on

insert into [dbo].[RECEPTIONS] ([id], [type], [patient_id], [date])
    values (1, 0, 1, '2016-04-04 11:00');
insert into [dbo].[RECEPTIONS] ([id], [type], [patient_id], [date])
    values (2, 0, 2, '2016-04-03 13:20');
insert into [dbo].[RECEPTIONS] ([id], [type], [patient_id], [date])
    values (3, 0, 4, '2016-04-05 17:30');
insert into [dbo].[RECEPTIONS] ([id], [type], [patient_id], [date])
    values (4, 0, 5, '2016-03-04 12:30');
insert into [dbo].[RECEPTIONS] ([id], [type], [patient_id], [date])
    values (5, 0, 5, '2016-03-07 13:15');
insert into [dbo].[RECEPTIONS] ([id], [type], [patient_id], [date])
    values (6, 0, 3, '2016-04-01 18:30');
insert into [dbo].[RECEPTIONS] ([id], [type], [patient_id], [date])
    values (7, 0, 1, '2016-04-05 10:30');
insert into [dbo].[RECEPTIONS] ([id], [type], [patient_id], [date])
    values (8, 1, 5, '2016-01-04 11:40');
insert into [dbo].[RECEPTIONS] ([id], [type], [patient_id], [date])
    values (9, 0, 3, '2016-03-09 13:45');
insert into [dbo].[RECEPTIONS] ([id], [type], [patient_id], [date])
    values (10, 0, 3, '2016-04-01 18:30');
insert into [dbo].[RECEPTIONS] ([id], [type], [patient_id], [date])
    values (11, 1, 3, '2016-02-09 12:35');
insert into [dbo].[RECEPTIONS] ([id], [type], [patient_id], [date])
    values (12, 0, 3, '2016-04-06 14:40');

set identity_insert [dbo].[RECEPTIONS] off

set identity_insert [dbo].[HEALTH_PROFESSIONALS] on
insert into [dbo].[HEALTH_PROFESSIONALS] ([id], [full_name], [post], [comment])
    values (1, 'Bokeria Leo Antonovich', 'heart surgeon', 'Doctor of Medical Sciences, over 40 years'' experience');
insert into [dbo].[HEALTH_PROFESSIONALS] ([id], [full_name], [post], [comment])
    values (2, 'Soldatov Igor Vladimirovich', 'mammolog, oncologist', 'PhD, the highest category, over 30 years'' experience');
insert into [dbo].[HEALTH_PROFESSIONALS] ([id], [full_name], [post], [comment])
    values (3, 'Nekoval Valeriy Mihaylovich', 'oncologist, proctologist, surgeon', 'The highest category, over 15 years'' experience');
insert into [dbo].[HEALTH_PROFESSIONALS] ([id], [full_name], [post], [comment])
    values (4, 'Teschina Oksana Gennadevna', 'doctor of ultrasonic diagnostics', 'The highest category, over 18 years'' experience');
insert into [dbo].[HEALTH_PROFESSIONALS] ([id], [full_name], [post], [comment])
    values (5, 'Efremova Elena Vitalevna', 'nurse, massage therapist', 'The highest category, over 10 years'' experience');
insert into [dbo].[HEALTH_PROFESSIONALS] ([id], [full_name], [post], [comment])
    values (6, 'Efremova Irina Vitalevna', 'nurse, beautician', 'The first category, over 10 years'' experience');
insert into [dbo].[HEALTH_PROFESSIONALS] ([id], [full_name], [post], [comment])
    values (7, 'Macsimov Ivan Petrovich', 'internist', 'The highest category, over 24 years'' experience');
insert into [dbo].[HEALTH_PROFESSIONALS] ([id], [full_name], [post], [comment])
    values (8, 'Isaev Roman Anatolevich', 'ophthalmologist', 'The highest category, over 15 years'' experience');

set identity_insert [dbo].[HEALTH_PROFESSIONALS] off


set identity_insert [dbo].[DIAGNOSES] on
insert into [dbo].[DIAGNOSES] ([id], [name], [description], [type], [patient_id])
  values (1, 'cold', 'The common cold, including chest cold and head cold, and seasonal flu are caused by viruses.',
          1, 1);
insert into [dbo].[DIAGNOSES] ([id], [name], [description], [type], [patient_id])
  values (2, 'Blepharitis', 'Front boundary Ulcerative Blepharitis.',
          0, 1);
set identity_insert [dbo].[DIAGNOSES] off

set identity_insert [dbo].[RECORDS] on
insert into [dbo].[RECORDS] ([id], [writing], [patient_id], [diagnosis_id], [diagnosis_state], [reception_id], [health_professional_id])
  values (1, 'Headache and lacrimation are observed. The skin is normal.
   Low-grade temperature. The patient complains of weakness, lethargy, loss of appetite, sore throat.',
          1, 1, '', 1, 7);
insert into [dbo].[RECORDS] ([id], [writing], [patient_id], [diagnosis_id], [reception_id], [health_professional_id])
  values (2, 'Purulent inflammation of the eyelash hair follicles and the formation of ulcers on the edge of the eyelids.',
        1, 2, 7, 8);
set identity_insert [dbo].[RECORDS] off

set identity_insert [dbo].[ADDITIONAL_INFO] on
insert into [dbo].[ADDITIONAL_INFO] ([id], [patient_id], [info_type], [info])
    values(1, 1, 0, 'AB+');
insert into [dbo].[ADDITIONAL_INFO] ([id], [patient_id], [info_type], [info])
    values(2, 1, 1, 'Allergy to cold.  Indication: rhinitis, headache, conjunctivitis');
insert into [dbo].[ADDITIONAL_INFO] ([id], [patient_id], [info_type], [info])
    values(3, 1, 2, 'Antibiotics penicillin series');
insert into [dbo].[ADDITIONAL_INFO] ([id], [patient_id], [info_type], [info])
    values(4, 1, 2, 'Preparations containing iodine');
set identity_insert [dbo].[ADDITIONAL_INFO] off

set IDENTITY_INSERT [dbo].[SERVICES] on
insert into [dbo].[SERVICES]([id],[type], [name])
    values(1, 1, 'Neck massage');
insert into [dbo].[SERVICES]([id],[type], [name])
    values(2, 1, 'Bandaging');
insert into [dbo].[SERVICES]([id],[type], [name])
    values(3, 1, 'Gypsum overlay');
insert into [dbo].[SERVICES]([id],[type], [name])
    values(4, 1, 'Enema');
insert into [dbo].[SERVICES]([id],[type], [name])
    values(5, 1, 'Injection');
insert into [dbo].[SERVICES]([id],[type], [name])
    values(6, 1, 'Dropper');
insert into [dbo].[SERVICES]([id],[type], [name])
    values(7, 0, 'Validol in pills, 1 pill');
insert into [dbo].[SERVICES]([id],[type], [name])
    values(8, 0, 'Validol in drops, 30 ml');
set identity_insert [dbo].[SERVICES] off

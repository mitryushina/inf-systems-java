
create table [dbo].[COURSE_COMPLEXITIES] (
  [id] bigint identity (1, 1) not null,
  [title] nvarchar(1024) not null,

  constraint [PK_COURSE_COMPLEXITY] primary key clustered ([id] asc)
)
go

create table [dbo].[COURSES] (
  [id] bigint identity (1, 1) not null,
  [code] nvarchar(255) not null,
  [title] nvarchar(1024) not null,
  [complexity_id] bigint not null,

  constraint [PK_COURSE] primary key clustered ([id] asc)
)
go

alter table [dbo].[COURSES] add constraint [FK_COURSE_COURSE_COMPLEXITY]
foreign key ([complexity_id]) references [dbo].[COURSE_COMPLEXITIES] ([id])
go

create table [dbo].[CONTACTS](
  [id] bigint identity(1, 1) not null,
  [address] nvarchar(1024),
  [phone_number] nvarchar(255),
  [email] nvarchar(255),

    constraint [PR_CONTACT] primary key clustered ([id] asc)
)
go

create table [dbo].[PATIENTS] (
  [id] bigint identity (1, 1) not null,
  [full_name] nvarchar(1024) not null,
  [birth_date] date not null,
  [contact_id] bigint not null,

    constraint [PK_PATIENT] primary key clustered ([id] asc)
)
go

alter table [dbo].[PATIENTS] add constraint [FK_PATIENT_CONTACT]
foreign key ([contact_id]) references [dbo].[CONTACTS] ([id])
go

create table [dbo].[ADDITIONAL_INFO](
  [id] bigint identity (1, 1) not null,
  [patient_id] bigint not null,
  [info_type] integer not null,
  [info] NVARCHAR(1024) not null,

    constraint [PK_ADDITIONAL_INFO] primary key clustered ([id] asc)
)
go

alter table [dbo].ADDITIONAL_INFO add constraint [FK_ADDITIONAL_INFO_PATIENT]
foreign key ([patient_id]) references [dbo].[PATIENTS] ([id])
go

create table [dbo].[HEALTH_PROFESSIONALS](
  [id] bigint identity (1, 1) not null,
  [full_name] nvarchar(255) not null,
  [post] nvarchar(255) not null,
  [comment] nvarchar(1024) not null,

    constraint [PK_HEALTH_PROFESSIONAL] primary key clustered ([id] asc)
)
go

create table [dbo].[DIAGNOSES](
  [id] bigint identity (1, 1) not null,
  [name] nvarchar(255) not null,
  [description] nvarchar(1024),
  [type] nvarchar(255) not null,
  [patient_id] bigint not null,

    constraint [PK_DIAGNOSIS] primary key clustered ([id] asc)
)
go

alter table [dbo].[DIAGNOSES] add constraint [FK_DIAGNOSIS_PATIENT]
foreign key ([patient_id]) references [dbo].[PATIENTS] ([id])
go

create table [dbo].[RECEPTIONS](
  [id] bigint identity (1, 1) not null,
  [type] integer not null,
  [patient_id] bigint not null,
  [date] datetime not null,

    constraint [PK_RECEPTION] primary key clustered ([id] asc)
)
go

alter table [dbo].RECEPTIONS add constraint [FK_RECEPTION_PATIENT]
foreign key ([patient_id]) references [dbo].[PATIENTS] ([id])
go

create table [dbo].[RECORDS](
  [id] bigint identity (1, 1) not null,
  [writing] nvarchar(1024),
  [patient_id] bigint not null,
  [diagnosis_id] bigint not null,
  [diagnosis_state] nvarchar(255) not null,
  [reception_id] bigint not null,
  [health_professional_id] bigint not null,

    constraint [PK_RECORD] primary key clustered ([id] asc)
)
go

alter table [dbo].[RECORDS] add constraint [FK_RECORD_PATIENT]
foreign key ([patient_id]) references [dbo].[PATIENTS] ([id])
go

alter table [dbo].[RECORDS] add constraint [FK_RECORD_DIAGNOSIS]
foreign key ([diagnosis_id]) references [dbo].[DIAGNOSES] ([id])
go

alter table [dbo].[RECORDS] add constraint [FK_RECORD_RECEPTION]
foreign key ([reception_id]) references [dbo].[RECEPTIONS] ([id])
go

alter table [dbo].[RECORDS] add constraint [FK_RECORD_HEALTH_PROFESSIONAL]
foreign key ([health_professional_id]) references [dbo].[HEALTH_PROFESSIONALS] ([id])
go


create table [dbo].[ATTACHMENTS](
  [id] bigint identity (1, 1) not null,
  [record_id] bigint not null,
  [name] nvarchar(255) not null,
  [description] nvarchar(1024),

  constraint [PK_ATTACHMENT] primary key clustered ([id] asc)
)
go

alter table [dbo].[ATTACHMENTS] add constraint [FK_ATTACHMENT_RECORD]
foreign key ([record_id]) references [dbo].[RECORDS] ([id])
go

create table [dbo].[SERVICES](
  [id] bigint identity (1, 1) not null,
  [type] integer not null,
  [name] nvarchar(255) not null,

  constraint [PK_SERVICE] primary key clustered ([id] asc)

)
go

create table [dbo].[RECORDS_SERVICES](
  [id] bigint identity (1, 1) not null,
  [record_id] bigint not null,
  [service_id] bigint not null,
  [amount] integer not null,

    constraint [PK_RECORDS_SERVICES] primary key clustered ([id] asc)

)
go

alter table [dbo].[RECORDS_SERVICES] add constraint [FK_RECORDS_SERVICES_RECORD]
foreign key ([record_id]) references [dbo].[RECORDS] ([id])
go

alter table [dbo].[RECORDS_SERVICES] add constraint [FK_RECORDS_SERVICES_SERVICE]
foreign key ([service_id]) references [dbo].[SERVICES] ([id])
go


create table [dbo].[TREATMENT_TYPES](
  [id] bigint identity(1, 1) not null,
  [name] nvarchar(255) not null,
  [description] nvarchar(1024),

  constraint [PR_TREATMENT_TYPE] primary key clustered ([id] asc)
)
go

create table [dbo].[TREATMENTS](
  [id] bigint identity (1, 1) not null,
  [treatment_type_id] bigint not null,
  [record_id] bigint not null,
  [health_professional_id] bigint not null,
  [progress] integer not null,

  constraint [PK_TREATMENT] primary key clustered ([id] asc)

)
go

alter table [dbo].[TREATMENTS] add constraint [FK_TREATMENT_TREATMENT_TYPE]
foreign key ([treatment_type_id]) references [dbo].[TREATMENT_TYPES] ([id])
go

alter table [dbo].[TREATMENTS] add constraint [FK_TREATMENT_RECORD]
foreign key ([record_id]) references [dbo].[RECORDS] ([id])
go

alter table [dbo].[TREATMENTS] add constraint [FK_TREATMENT_HEALTH_PROFESSIONAL]
foreign key ([health_professional_id]) references [dbo].[HEALTH_PROFESSIONALS] ([id])
go

create table [dbo].[PASSPORTS](
  [id] bigint identity (1, 1) not null,
  [series] nvarchar(255) not null,
  [number] nvarchar(255) not null,
  [issuing_department]  nvarchar(255) not null,
  [issue_date] date not null,
  [patient_id] bigint not null,

  constraint [PK_PASSPORT] primary key clustered ([id] asc)

)
go

alter table [dbo].[PASSPORTS] add constraint [FK_PASSPORT_PATIENT]
foreign key ([patient_id]) references [dbo].[PATIENTS] ([id])
go

create table [dbo].[POLICIES](
  [id] bigint identity (1, 1) not null,
  [series] nvarchar(255) not null,
  [number] nvarchar(255) not null,
  [company] nvarchar(255) not null,
  [patient_id] bigint not null,

  constraint [PK_POLICY] primary key clustered ([id] asc)

)
go

alter table [dbo].[POLICIES] add constraint [FK_POLICY_PATIENT]
foreign key ([patient_id]) references [dbo].[PATIENTS] ([id])
go








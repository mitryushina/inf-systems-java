package org.hecsit.infsys.educenter.core.courses;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class CoursesApi {

    private final SessionFactory sessionFactory;

    @Autowired
    public CoursesApi(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Course> getCourses() {
        Session session = sessionFactory.getCurrentSession();
        List<Course> courses = (List<Course>)session.createQuery("from Course c join fetch c.complexity")
                .list();
        return courses;
    }

    public List<CourseComplexity> getCourseComplexities() {
        Session session = sessionFactory.getCurrentSession();
        return (List<CourseComplexity>)session.createQuery("from CourseComplexity")
                .list();
    }

    public void createCourse(String code, String title, long complexityId) {
        Session session = sessionFactory.getCurrentSession();
        CourseComplexity complexity = session.byId(CourseComplexity.class).getReference(complexityId);
        Course course = new Course(code, title, complexity);
        session.save(course);
    }
}

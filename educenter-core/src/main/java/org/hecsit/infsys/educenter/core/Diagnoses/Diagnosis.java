package org.hecsit.infsys.educenter.core.diagnoses;

import org.hecsit.infsys.educenter.core.BaseEntity;
import org.hecsit.infsys.educenter.core.patients.Patient;
import javax.persistence.*;

@javax.persistence.Entity
@javax.persistence.Table(name = "DIAGNOSES")
public class Diagnosis extends BaseEntity {
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description", length = 1024)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false, length = 255)
    private DiagnosisType type;

    @ManyToOne
    @JoinColumn(name = "patient_id", nullable = false)
    private Patient patient;

    public Diagnosis(){}

    public Diagnosis(String name, String description, DiagnosisType type, Patient patient){
        this.name = name;
        this.description = description;
        this.type = type;
        this.patient = patient;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getDescription(){
        return description;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public  DiagnosisType getType(){
        return type;
    }

    public void setType(DiagnosisType type){
        this.type = type;
    }

    public Patient getPatient(){
        return patient;
    }

    public void setPatient(Patient patient){
        this.patient = patient;
    }

}

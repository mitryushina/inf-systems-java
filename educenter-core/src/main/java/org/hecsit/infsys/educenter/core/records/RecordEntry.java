package org.hecsit.infsys.educenter.core.records;

/**
 * Created by Наталья on 21.09.2016.
 */
public class RecordEntry {
    private Long Id;

    private String Writing;

    private String Patient;

    private String Diagnosis;

    private String DiagnosisState;

    private Long ReceptionId;

    private String HealthProfessional;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getWriting() {
        return Writing;
    }

    public void setWriting(String writing) {
        Writing = writing;
    }

    public String getPatient() {
        return Patient;
    }

    public void setPatient(String patient) {
        Patient = patient;
    }

    public String getDiagnosis() {
        return Diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        Diagnosis = diagnosis;
    }

    public String getDiagnosisState() {
        return DiagnosisState;
    }

    public void setDiagnosisState(String diagnosisState) {
        DiagnosisState = diagnosisState;
    }

    public Long getReceptionId() {
        return ReceptionId;
    }

    public void setReceptionId(Long receptionId) {
        ReceptionId = receptionId;
    }

    public String getHealthProfessional() {
        return HealthProfessional;
    }

    public void setHealthProfessional(String healthProfessional) {
        HealthProfessional = healthProfessional;
    }
}

package org.hecsit.infsys.educenter.core.diagnoses;

import org.hecsit.infsys.educenter.core.patients.Patient;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Наталья on 05.04.2016.
 */
@Service
@Transactional
public class DiagnosesApi {
    private final SessionFactory sessionFactory;

    @Autowired
    public DiagnosesApi(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Diagnosis getDiagnosisById(Long diagnosisId){
        Session session = sessionFactory.getCurrentSession();
        Diagnosis diagnosis = (Diagnosis)session.createQuery("from Diagnosis d where d.id = :diagnosisId")
                .setParameter("diagnosisId", diagnosisId)
                .uniqueResult();
        return diagnosis;
    }

    public Diagnosis createDiagnosis(String name, String description, String type, Patient patient) {
        Session session = sessionFactory.getCurrentSession();

        Diagnosis diagnosis = new Diagnosis(name, description, DiagnosisType.valueOf(type), patient);
        session.save(diagnosis);
        return diagnosis;
    }

}

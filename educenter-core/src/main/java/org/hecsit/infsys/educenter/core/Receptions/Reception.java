package org.hecsit.infsys.educenter.core.receptions;

import org.hecsit.infsys.educenter.core.BaseEntity;
import org.hecsit.infsys.educenter.core.patients.Patient;

import java.util.Date;
import javax.persistence.*;

/**
 * Created by Наталья on 01.03.2016.
 */
@javax.persistence.Entity
@Table(name = "RECEPTIONS")
public class Reception extends BaseEntity {
    public enum ReceptionType{
        Open,
        Close
    }

    @Column(name = "type", nullable = false)
    private ReceptionType receptionType = ReceptionType.Open;

    @ManyToOne
    @JoinColumn(name = "patient_id", nullable = false)
    private Patient patient;

    @Column(name = "date", nullable = false)
    private Date date;

    public Reception() {}

    public Reception(Patient patient, Date date){
        this.patient = patient;
        this.date = date;
    }

    public ReceptionType getReceptionType(){ return receptionType; }

    public void setReceptionType(ReceptionType receptionType){ this.receptionType = receptionType; }

    public Patient getPatient(){
        return patient;
    }

    public void setPatient(Patient patient){
        this.patient = patient;
    }

    public Date getDate(){
        return date;
    }

    public void setDate(Date date){
        this.date = date;
    }
}

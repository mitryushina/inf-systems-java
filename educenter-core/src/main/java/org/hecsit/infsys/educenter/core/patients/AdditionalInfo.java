package org.hecsit.infsys.educenter.core.patients;

import org.hecsit.infsys.educenter.core.BaseEntity;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by Наталья on 01.03.2016.
 */
@javax.persistence.Entity
@Table(name = "ADDITIONAL_INFO")
public class AdditionalInfo extends BaseEntity {
    public enum AdditionalInfoType{
        BloodGroup,
        AllergicResponse,
        Intolerance
    }

    @ManyToOne
    @JoinColumn(name = "patient_id", nullable = false)
    private Patient patient;

    @Column(name = "info_type", nullable = false)
    private AdditionalInfoType infoType;

    @Column(name = "info", length = 1024, nullable = false)
    private String info;

    public AdditionalInfo(){}

    public AdditionalInfo(Patient patient, AdditionalInfoType infoType, String info){
        this.patient = patient;
        this.infoType = infoType;
        this.info = info;
    }

    public Patient getPatient(){ return patient; }

    public  void setPatient(Patient patient){
        this.patient = patient;
    }

    public AdditionalInfoType getInfoType(){ return infoType; }

    public void setInfoType(AdditionalInfoType infoType){ this.infoType = infoType; }

    public String getInfo(){ return info; }

    public  void setInfo(String info){ this.info = info; }

}

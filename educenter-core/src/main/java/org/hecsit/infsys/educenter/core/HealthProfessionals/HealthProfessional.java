package org.hecsit.infsys.educenter.core.healthProfessionals;

import org.hecsit.infsys.educenter.core.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * Created by Наталья on 01.03.2016.
 */
@javax.persistence.Entity
@Table(name = "HEALTH_PROFESSIONALS")
public class HealthProfessional extends BaseEntity {
    @Column(name = "full_name", nullable = false)
    private String fullName;

    @Column(name = "post", nullable = false)
    private String post;

    @Column(name = "comment", length = 1024, nullable = false)
    private String comment;

    public HealthProfessional(){}

    public HealthProfessional(String fullName, String post, String comment){
        this.fullName = fullName;
        this.post = post;
        this.comment = comment;
    }

    public String getFullName(){
        return fullName;
    }

    public void setFullName(String fullName){
        this.fullName = fullName;
    }

    public String getPost(){
        return post;
    }

    public void setPost(String post){
        this.post = post;
    }

    public  String getComment(){ return comment; }

    public  void setComment(String comment){ this.comment = comment; }
}

package org.hecsit.infsys.educenter.core.diagnoses;

public enum DiagnosisType{
    NOT_FINAL,
    FINAL
}
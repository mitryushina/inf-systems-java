package org.hecsit.infsys.educenter.core.workflow;

public enum DiagnosisConfirmation {
    SAVE_CONFIRMED,
    KEEP_UNDEFINED,
    REVISION,
    SAVE_CONCLUDED,
    REJECTED
}

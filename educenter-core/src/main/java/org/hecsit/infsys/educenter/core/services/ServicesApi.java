package org.hecsit.infsys.educenter.core.services;

import org.hecsit.infsys.educenter.core.patients.AdditionalInfo;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@org.springframework.stereotype.Service
@Transactional
public class ServicesApi {
    private final SessionFactory sessionFactory;

    @Autowired
    public ServicesApi(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Service> getServices(){
        Session session = sessionFactory.getCurrentSession();
        List<Service> services = (List<Service>)session.createQuery("from Service s")
                .list();
        return services;
    }
}

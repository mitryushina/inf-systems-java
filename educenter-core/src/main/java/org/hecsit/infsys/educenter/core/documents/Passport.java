package org.hecsit.infsys.educenter.core.documents;

import org.hecsit.infsys.educenter.core.patients.Patient;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Наталья on 01.03.2016.
 */
@javax.persistence.Entity
@Table(name = "PASSPORTS")
public class Passport extends Document {
    @Column(name = "issuing_department", nullable = false)
    private String issuingDepartment;

    @Column(name = "issue_date", nullable = false)
    @Temporal(value = TemporalType.DATE)
    private Date issueDate;

    @OneToOne
    @JoinColumn(name = "patient_id", nullable = false)
    private Patient patient;

    public Passport(){}

    public Passport(String series, String number, String issuingDepartment, Date issueDate, Patient patient){
        super.series = series;
        super.number = number;
        this.issuingDepartment = issuingDepartment;
        this.issueDate = issueDate;
        this.patient = patient;
    }

    public String getIssuingDepartment(){
        return issuingDepartment;
    }

    public void setIssuingDepartment(String issuingDepartment){
        this.issuingDepartment = issuingDepartment;
    }

    public Date getIssueDate(){
        return issueDate;
    }

    public void setIssueDate(Date issueDate){
        this.issueDate = issueDate;
    }

    public Patient getPatient(){
        return patient;
    }

    public void setPatient(Patient patient){
        this.patient = patient;
    }
}

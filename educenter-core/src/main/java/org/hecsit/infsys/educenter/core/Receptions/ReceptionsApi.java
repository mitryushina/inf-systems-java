package org.hecsit.infsys.educenter.core.receptions;

import org.hecsit.infsys.educenter.core.patients.Patient;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.omg.CORBA.DATA_CONVERSION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Наталья on 05.04.2016.
 */
@Service
@Transactional
public class ReceptionsApi {
    private final SessionFactory sessionFactory;

    @Autowired
    public ReceptionsApi(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Reception getReception(Long id){
        Session session = sessionFactory.getCurrentSession();
        Reception reception = (Reception)session.createQuery("from Reception r where r.id = :id")
                .setParameter("id", id)
                .uniqueResult();
        return reception;
    }

    public List<Reception> getReceptions() {
        Session session = sessionFactory.getCurrentSession();
        List<Reception> receptions = (List<Reception>)session.createQuery("from Reception r")
                .list();
        return receptions;
    }

    public  Long getPatientId(Long receptionId){
        Session session = sessionFactory.getCurrentSession();
        Long patientId = (Long)session.createQuery("select r.patient.id from Reception r where r.id = :receptionId")
                .setParameter("receptionId", receptionId)
                .uniqueResult();
        return patientId;
    }

    public Date getDate(Long receptionId){
        Session session = sessionFactory.getCurrentSession();
        Date date = (Date)session.createQuery("select r.date from Reception r where r.id = :receptionId")
                .setParameter("receptionId", receptionId)
                .uniqueResult();
        return date;
    }

    public void changeType(Long receptionId){
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("update Reception r set r.receptionType = :receptionType where r.id = :receptionId")
                .setParameter("receptionType", Reception.ReceptionType.Close)
                .setParameter("receptionId", receptionId);
        query.executeUpdate();

    }
}

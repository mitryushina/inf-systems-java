package org.hecsit.infsys.educenter.core.documents;

import org.hecsit.infsys.educenter.core.BaseEntity;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * Created by Наталья on 01.03.2016.
 */
@MappedSuperclass
public abstract class Document extends BaseEntity {
    @Column(name = "series", nullable = false)
    protected String series;

    @Column(name = "number", nullable = false)
    protected String number;


    public String getSeries(){
        return series;
    }

    public void setSeries(String series){
        this.series = series;
    }

    public String getNumber(){
        return number;
    }

    public void setNumber(String number){
        this.number = number;
    }
}


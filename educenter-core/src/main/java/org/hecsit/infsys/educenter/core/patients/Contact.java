package org.hecsit.infsys.educenter.core.patients;

import org.hecsit.infsys.educenter.core.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * Created by Наталья on 01.03.2016.
 */
@javax.persistence.Entity
@Table(name = "CONTACTS")
public class Contact extends BaseEntity {
    @Column(name = "address", length = 1024)
    private String address;

    @Column(name = "phone_Number")
    private String phoneNumber;

    @Column(name = "email")
    private  String email;

    public Contact(){}

    public Contact(String address, String phoneNumber, String email){
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    public String getAddress(){
        return address;
    }

    public void setAddress(String phoneNumber){ this.phoneNumber = phoneNumber; }

    public String getPhoneNumber(){
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber){ this.phoneNumber = phoneNumber; }

    public  String getEmail(){
        return email;
    }

    public void setEmail(String email){ this.email = email; }
}

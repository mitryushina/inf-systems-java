package org.hecsit.infsys.educenter.core.records;

public enum DiagnosisState {
    NO_NEED_APPROVAL,
    NEED_APPROVAL,
    CONFIRMED,
    REJECTED
}

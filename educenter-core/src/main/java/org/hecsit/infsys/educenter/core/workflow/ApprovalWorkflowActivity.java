package org.hecsit.infsys.educenter.core.workflow;

import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ApprovalWorkflowActivity {

    private final SessionFactory sessionFactory;

    @Autowired
    public ApprovalWorkflowActivity(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void saveConfirmed(Long diagnosisId){
        Session session = sessionFactory.getCurrentSession();
    }

    public void keepUndefined(Long diagnosisId){

    }

    public void saveConcluded(Long diagnosisId){

    }

    public void close(Long diagnosisId){

    }


    /*
    public void recordConfirmation(DelegateExecution execution, Long recordId, String resolution) {
        Session session = sessionFactory.getCurrentSession();
        Record record = session.byId(Record.class).getReference(recordId);
        record.setDiagnosisState(DiagnosisState.valueOf(resolution));
    }

    public void diagnosisConfirmation(DelegateExecution execution, Long diagnosisId) {
        Session session = sessionFactory.getCurrentSession();
        Diagnosis diagnosis = session.byId(Diagnosis.class).getReference(diagnosisId);
        diagnosis.setType(DiagnosisType.FINAL);
    }

    public void diagnosisRejection(DelegateExecution execution, Long diagnosisId) {
        Session session = sessionFactory.getCurrentSession();
        Diagnosis diagnosis = session.byId(Diagnosis.class).getReference(diagnosisId);
        diagnosis.setType(DiagnosisType.NOT_FINAL);
    }
    */
}

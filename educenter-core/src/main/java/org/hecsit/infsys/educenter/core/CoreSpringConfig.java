package org.hecsit.infsys.educenter.core;

import com.zaxxer.hikari.HikariDataSource;
import org.activiti.engine.*;
import org.activiti.spring.ProcessEngineFactoryBean;
import org.activiti.spring.SpringProcessEngineConfiguration;
import org.hecsit.infsys.educenter.core.workflow.ApprovalWorkflowActivity;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Configuration
@ComponentScan(basePackages = "org.hecsit.infsys.educenter.core")
@EnableTransactionManagement
@PropertySource({"classpath:datasource.properties"})
public class CoreSpringConfig {

    @Autowired
    private Environment env;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    @Autowired
    public LocalSessionFactoryBean sessionFactory(DataSource dataSource) throws PropertyVetoException {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource);
        sessionFactory.setPackagesToScan("org.hecsit.infsys.educenter.core");
        sessionFactory.setHibernateProperties(hibernateProperties());

        return sessionFactory;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    @Bean
    public DataSource dataSource() throws PropertyVetoException {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setDriverClassName(env.getProperty("jdbc.driverClassName"));
        dataSource.setJdbcUrl(env.getProperty("jdbc.url"));
        dataSource.setUsername(env.getProperty("jdbc.user"));
        dataSource.setPassword(env.getProperty("jdbc.password"));

        dataSource.setConnectionTimeout(15*1000);
        dataSource.setMaxLifetime(15*60*1000);
        dataSource.setIdleTimeout(2*60*1000);

        dataSource.setMaximumPoolSize(100); // Default: 10
        dataSource.setMinimumIdle(50); // Default: Same as `maximumPoolSize`

        return dataSource;
    }

    @Bean
    @Autowired
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(sessionFactory);

        return txManager;
    }

    private Properties hibernateProperties() {
        return new Properties() {
            {
                setProperty("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
                setProperty("hibernate.dialect", env.getProperty("hibernate.dialect"));
                setProperty("hibernate.globally_quoted_identifiers", env.getProperty("hibernate.globally_quoted_identifiers"));
                setProperty("hibernate.id.new_generator_mappings", env.getProperty("hibernate.id.new_generator_mappings"));
            }
        };
    }

    @Bean
    public SpringProcessEngineConfiguration processEngineConfiguration(DataSource dataSource,
                                                                       PlatformTransactionManager transactionManager,
                                                                       ApprovalWorkflowActivity approvalWorkflowActivity)
            throws IOException {
        SpringProcessEngineConfiguration configuration = new SpringProcessEngineConfiguration();
        configuration.setDataSource(dataSource);
        configuration.setTransactionManager(transactionManager);
        configuration.setDatabaseSchemaUpdate("false");
        //configuration.setJobExecutorActivate(true);
        configuration.setAsyncExecutorActivate(true);
        configuration.setDeploymentMode("single-resource");

        ResourcePatternResolver patternResolver = new PathMatchingResourcePatternResolver();
        Resource[] processes = patternResolver.getResources("classpath*:/bpmn/*.xml");
        configuration.setDeploymentResources(processes);

        Map<Object, Object> beansMap = new HashMap<>();
        beansMap.put("diagnosis_confirmation", approvalWorkflowActivity);
        configuration.setBeans(beansMap);

        return configuration;
    }

    @Bean
    public ProcessEngineFactoryBean processEngineFactoryBean(SpringProcessEngineConfiguration processEngineConfiguration) {
        ProcessEngineFactoryBean bean = new ProcessEngineFactoryBean();
        bean.setProcessEngineConfiguration(processEngineConfiguration);
        return bean;
    }

    @Bean
    public RepositoryService repositoryService(ProcessEngineFactoryBean processEngineFactoryBean) throws Exception{
        return processEngineFactoryBean.getObject()
                .getRepositoryService();
    }

    @Bean
    public RuntimeService runtimeService(ProcessEngineFactoryBean processEngineFactoryBean) throws Exception {
        return processEngineFactoryBean.getObject()
                .getRuntimeService();
    }

    @Bean
    public HistoryService historyService(ProcessEngineFactoryBean processEngineFactoryBean) throws Exception {
        return processEngineFactoryBean.getObject()
                .getHistoryService();
    }

    @Bean
    public ManagementService managementService(ProcessEngineFactoryBean processEngineFactoryBean) throws Exception {
        return processEngineFactoryBean.getObject()
                .getManagementService();
    }

    @Bean
    public IdentityService identityService(ProcessEngineFactoryBean processEngineFactoryBean) throws Exception {
        return processEngineFactoryBean.getObject()
                .getIdentityService();
    }

    @Bean
    public FormService formService(ProcessEngineFactoryBean processEngineFactoryBean) throws Exception {
        return processEngineFactoryBean.getObject()
                .getFormService();
    }

    @Bean
    public TaskService taskService(ProcessEngineFactoryBean processEngineFactoryBean) throws Exception {
        return processEngineFactoryBean.getObject()
                .getTaskService();
    }
}

package org.hecsit.infsys.educenter.core.healthProfessionals;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Наталья on 05.04.2016.
 */
@Service
@Transactional
public class HealthProfessionalsApi {
    private final SessionFactory sessionFactory;

    @Autowired
    public HealthProfessionalsApi(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public HealthProfessional getHealthProfessionalById(Long healthProfessionalId){
        Session session = sessionFactory.getCurrentSession();
        HealthProfessional healthProfessional = (HealthProfessional)session.createQuery("from HealthProfessional h where h.id = :healthProfessionalId")
                .setParameter("healthProfessionalId", healthProfessionalId)
                .uniqueResult();
        return healthProfessional;
    }
}

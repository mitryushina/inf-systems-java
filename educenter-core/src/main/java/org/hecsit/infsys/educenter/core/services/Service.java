package org.hecsit.infsys.educenter.core.services;

import org.hecsit.infsys.educenter.core.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * Created by Наталья on 01.03.2016.
 */
@javax.persistence.Entity
@Table(name = "SERVICES")
public class Service extends BaseEntity {
    public enum ServiceType{
        Drug,
        Treatment
    }

    @Column(name = "type", nullable = false)
    private ServiceType serviceType;

    @Column(name = "name", nullable = false)
    private String name;

    public Service(){}

    public Service(ServiceType serviceType, String name){
        this.serviceType = serviceType;
        this.name = name;
    }

    public ServiceType getServiceType(){
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType){
        this.serviceType = serviceType;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

}

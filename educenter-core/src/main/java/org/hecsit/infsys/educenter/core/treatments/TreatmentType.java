package org.hecsit.infsys.educenter.core.treatments;

import org.hecsit.infsys.educenter.core.BaseEntity;

import javax.persistence.*;

/**
 * Created by Наталья on 01.03.2016.
 */
@javax.persistence.Entity
@Table(name = "TREATMENT_TYPES")
public class TreatmentType extends BaseEntity{
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "description", length = 1024)
    private String description;

    public TreatmentType(String name, String description){
        this.name = name;
        this.description = description;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getDescription(){
        return description;
    }

    public void setDescription(String description){
        this.description = description;
    }
}

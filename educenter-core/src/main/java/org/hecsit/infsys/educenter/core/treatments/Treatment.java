package org.hecsit.infsys.educenter.core.treatments;

import org.hecsit.infsys.educenter.core.BaseEntity;
import org.hecsit.infsys.educenter.core.healthProfessionals.HealthProfessional;
import org.hecsit.infsys.educenter.core.records.Record;

import javax.persistence.*;

/**
 * Created by Наталья on 01.03.2016.
 */
@javax.persistence.Entity
@Table(name = "TREATMENTS")
public class Treatment extends BaseEntity {
    public enum ProgressType{
        Awaiting,
        InProgress,
        Completed,
        Uncompleted,
        Cancelled
    }
    @ManyToOne
    @JoinColumn(name = "treatment_type_id", nullable = false)
    private TreatmentType treatmentType;

    @ManyToOne
    @JoinColumn(name = "record_id", nullable = false)
    private Record record;

    @ManyToOne
    @JoinColumn(name = "health_professional_id", nullable = false)
    private HealthProfessional healthProfessional;

    @Column(name = "progress", nullable = false)
    private ProgressType progress;

    public Treatment(){}

    public  Treatment(TreatmentType treatmentType, Record record, HealthProfessional healthProfessional,
                      ProgressType progress){
        this.treatmentType = treatmentType;
        this.record = record;
        this.healthProfessional = healthProfessional;
        this.progress = progress;
    }

    public TreatmentType GetTreatmentType(){
        return treatmentType;
    }

    public void SetTreatmentType(TreatmentType treatmentType){
        this.treatmentType = treatmentType;
    }

    public Record GetRecord(){return record;}

    public void SetRecord(Record record){
        this.record = record;
    }

    public HealthProfessional GetHealthProfessional(){
        return healthProfessional;
    }

    public void SetHealthProfessional(HealthProfessional healthProfessional){
        this.healthProfessional = healthProfessional;
    }

    public ProgressType GetProgress(){
        return progress;
    }

    public void SetProgress(ProgressType progress){
        this.progress = progress;
    }
}

package org.hecsit.infsys.educenter.core.services;

import org.hecsit.infsys.educenter.core.BaseEntity;
import org.hecsit.infsys.educenter.core.records.Record;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by Наталья on 25.05.2016.
 */
@javax.persistence.Entity
@Table(name = "RECORDS_SERVICES")
public class ServiceProvided extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "record_id", nullable = false)
    private Record record;

    @ManyToOne
    @JoinColumn(name = "service_id", nullable = false)
    private Service service;

    @Column(name = "amount", nullable = false)
    private Integer amount;

    public ServiceProvided(){}

    public ServiceProvided(Record record, Service service, Integer amount){
        this.record = record;
        this.service = service;
        this.amount = amount;
    }

    public Record getRecord(){
        return record;
    }

    public void setRecord(Record record){
        this.record = record;
    }

    public Service getService(){
        return service;
    }

    public void setService(Service service){
        this.service = service;
    }

    public Integer getAmount(){
        return amount;
    }

    public void setAmount(Integer amount){
        this.amount = amount;
    }


}

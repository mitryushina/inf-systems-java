package org.hecsit.infsys.educenter.core.records;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.activiti.engine.RuntimeService;
import org.hecsit.infsys.educenter.core.diagnoses.Diagnosis;
import org.hecsit.infsys.educenter.core.diagnoses.DiagnosisType;
import org.hecsit.infsys.educenter.core.healthProfessionals.HealthProfessional;
import org.hecsit.infsys.educenter.core.patients.Patient;
import org.hecsit.infsys.educenter.core.receptions.Reception;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;


@Service
@Transactional
public class RecordsApi {
    private final SessionFactory sessionFactory;
    private final RuntimeService runtimeService;

    @Autowired
    public RecordsApi(SessionFactory sessionFactory, RuntimeService runtimeService) {
        this.sessionFactory = sessionFactory;
        this.runtimeService = runtimeService;
    }

    public List<Record> getRecords() {
        Session session = sessionFactory.getCurrentSession();
        List<Record> records = (List<Record>)session.createQuery("from Record r")
                .list();
        return records;
    }

    public List<Record> getRecordsByPatientId(Long patientId) {
        Session session = sessionFactory.getCurrentSession();
        List<Record> records = (List<Record>)session.createQuery("from Record r where r.patient.id = :patientId")
                .setParameter("patientId", patientId)
                .list();
        return records;
    }

    public Record getRecordByPatientId(Long patientId) {
        Session session = sessionFactory.getCurrentSession();
        Record record = (Record)session.createQuery("from Record r where r.patient.id = :patientId")
                .setParameter("patientId", patientId)
                .uniqueResult();
        return record;
    }

    public Record getRecordById(Long recordId) {
        Session session = sessionFactory.getCurrentSession();
        Record record = (Record)session.createQuery("from Record r where r.id = :recordId")
                .setParameter("recordId", recordId)
                .uniqueResult();
        return record;
    }

    public Long createRecord(String writing, Patient patient,
                             Diagnosis diagnosis, Reception reception, HealthProfessional healthProfessional) {
        Session session = sessionFactory.getCurrentSession();

        Record record = new Record(writing, patient, diagnosis, DiagnosisState.NEED_APPROVAL,
                reception, healthProfessional);
        session.save(record);
        if(diagnosis.getType().equals(DiagnosisType.NOT_FINAL)){
            Map<String, Object> variables = new HashMap<>();
            variables.put("diagnosisId", diagnosis.getId());
            variables.put("dueDate", Date.from(
                    LocalDate.now().plus(1, ChronoUnit.WEEKS).atStartOfDay(ZoneId.systemDefault()).toInstant()));


            runtimeService.startProcessInstanceByKey("diagnosis_confirmation", variables);
        }
        return record.getId();
    }

    /*public void addAttachment(Long recordId, String name, MultipartFile file) {
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();

                // Creating the directory to store file
                String rootPath = System.getProperty("catalina.home");
                File dir = new File(rootPath + File.separator + "tmpFiles");
                if (!dir.exists())
                    dir.mkdirs();

                // Create the file on server
                File serverFile = new File(dir.getAbsolutePath()
                        + File.separator + name);
                BufferedOutputStream stream = new BufferedOutputStream(
                        new FileOutputStream(serverFile));
                stream.write(bytes);
                stream.close();
            } catch (Exception e) {
            }
        }
        Record record = getRecordById(recordId);
        Attachment attachment = new Attachment(record, name, file);
        Session session = sessionFactory.getCurrentSession();
        session.save(attachment);
    }*/

    public void sendMessage(Long recordId) throws IOException, TimeoutException {
        Record record = getRecordById(recordId);
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = "";
        RecordEntry entry = new RecordEntry();
        entry.setId(record.getId());
        entry.setWriting(record.getWriting());
        entry.setDiagnosis(record.getDiagnosis().getName());
        entry.setDiagnosisState(record.getDiagnosisState().name());
        entry.setReceptionId(record.getReception().getId());
        entry.setHealthProfessional(record.getHealthProfessional().getFullName());
        try {
            json = ow.writeValueAsString(entry);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        com.rabbitmq.client.ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare("object1", false, false, false, null);
        String message = json;
        channel.basicPublish("", "object1", null, message.getBytes());

        channel.close();
        connection.close();
    }
}

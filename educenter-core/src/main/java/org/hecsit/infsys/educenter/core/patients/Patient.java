package org.hecsit.infsys.educenter.core.patients;

import org.hecsit.infsys.educenter.core.BaseEntity;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@javax.persistence.Entity
@Table(name = "PATIENTS")
public class Patient extends BaseEntity {
    @Column(name = "full_name", length = 255, nullable = false)
    private String fullName;

    @Column(name = "birth_date")
    @Temporal(value = TemporalType.DATE)
    private Date birthDate;

    @OneToOne
    @JoinColumn(name = "contact_id", nullable = false)
    private Contact contact;

    public Patient() {}

    public  Patient(String fullName, Date birthDate, Contact contact){
        this.fullName = fullName;
        this.birthDate = birthDate;
        this.contact = contact;
    }
    public String getFullName(){ return  fullName; }

    public void setFullName(String fullName){ this.fullName = fullName; }

    public Date getBirthDate(){ return birthDate; }

    public void setBirthDate(Date birthDate){ this.birthDate = birthDate; }

    public  Contact getContact(){ return contact; }

    public void setContact(Contact contact){ this.contact = contact; }

}

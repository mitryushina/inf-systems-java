package org.hecsit.infsys.educenter.core.patients;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Kate on 30.03.2016.
 */
@Service
@Transactional
public class PatientsApi {
    private final SessionFactory sessionFactory;

    @Autowired
    public PatientsApi(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Patient> getPatients() {
        Session session = sessionFactory.getCurrentSession();
        List<Patient> patients = (List<Patient>)session.createQuery("from Patient p")
                .list();
        return patients;
    }

    public Patient getPatientById(Long patientId){
        Session session = sessionFactory.getCurrentSession();
        Patient patient = (Patient)session.createQuery("from Patient p where p.id = :patientId")
                .setParameter("patientId", patientId)
                .uniqueResult();
        return patient;
    }

    public List<AdditionalInfo> getAdditionalInfos(Long patientId){
        Session session = sessionFactory.getCurrentSession();
        List<AdditionalInfo> infos = (List<AdditionalInfo>)session.createQuery("from AdditionalInfo a where a.patient.id = :patientId")
                .setParameter("patientId", patientId)
                .list();
        return infos;
    }
}

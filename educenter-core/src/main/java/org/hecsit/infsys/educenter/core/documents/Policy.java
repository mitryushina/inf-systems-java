package org.hecsit.infsys.educenter.core.documents;

/**
 * Created by Наталья on 01.03.2016.
 */

import org.hecsit.infsys.educenter.core.patients.Patient;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name = "POLICIES")
public class Policy extends Document {
    @Column(name = "company", nullable = false)
    private String company;

    @OneToOne
    @JoinColumn(name = "patient_id", nullable = false)
    private Patient patient;

    public Policy(){}

    public Policy(String series, String number, String company, Patient patient){
        super.series = series;
        super.number = number;
        this.company = company;
        this.patient = patient;
    }

    public String getCompany(){
        return company;
    }

    public void setCompany(String company){
        this.company = company;
    }

    public Patient getPatient(){
        return patient;
    }

    public void setPatient(Patient patient){
        this.patient = patient;
    }
}

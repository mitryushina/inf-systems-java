package org.hecsit.infsys.educenter.core.records;

import org.hecsit.infsys.educenter.core.BaseEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;

/**
 * Created by Наталья on 01.03.2016.
 */
@javax.persistence.Entity
@Table(name = "ATTACHMENTS")
public class Attachment extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "record_id", nullable = false)
    private Record record;

    @Column(name = "name", nullable = false)
    private String name;

   /* @Column(name = "file", nullable = false)
    private MultipartFile file;*/

    public Attachment(){}

    public Attachment(Record record, String name/*, MultipartFile file*/){
        this.record = record;
        this.name = name;
        /*this.file = file;*/
    }

    public Record getRecord(){
        return record;
    }

    public void setRecord(Record record){
        this.record = record;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

   /* public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }*/
}

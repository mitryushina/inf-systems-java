package org.hecsit.infsys.educenter.core.records;

import org.hecsit.infsys.educenter.core.BaseEntity;
import org.hecsit.infsys.educenter.core.diagnoses.Diagnosis;
import org.hecsit.infsys.educenter.core.healthProfessionals.HealthProfessional;
import org.hecsit.infsys.educenter.core.receptions.Reception;
import org.hecsit.infsys.educenter.core.patients.Patient;

import javax.persistence.*;

/**
 * Created by Наталья on 01.03.2016.
 */
@javax.persistence.Entity
@Table(name = "RECORDS")
public class Record extends BaseEntity {

    @Column(name = "writing", length = 1024)
    private String  writing;

    @ManyToOne
    @JoinColumn(name = "patient_id", nullable = false)
    private Patient patient;

    @ManyToOne
    @JoinColumn(name = "diagnosis_id", nullable = false)
    private Diagnosis diagnosis;

    @Enumerated(EnumType.STRING)
    @Column(name = "diagnosis_state", nullable = false, length = 255)
    private DiagnosisState diagnosisState;

    @ManyToOne
    @JoinColumn(name = "reception_id", nullable = false)
    private Reception reception;

    @ManyToOne
    @JoinColumn(name = "health_professional_id", nullable = false)
    private HealthProfessional healthProfessional;

    public Record(){}

    public Record(String writing, Patient patient, Diagnosis diagnosis,
                  DiagnosisState diagnosisState,
                  Reception reception, HealthProfessional healthProfessional) {
        this.writing = writing;
        this.patient = patient;
        this.diagnosis = diagnosis;
        this.diagnosisState = diagnosisState;
        this.reception = reception;
        this.healthProfessional = healthProfessional;
    }

    public String getWriting(){
        return writing;
    }

    public void setWriting(String writing){
        this.writing = writing;
    }

    public Patient getPatient(){
        return patient;
    }

    public void setPatient(Patient patient){
        this.patient = patient;
    }

    public Diagnosis getDiagnosis(){
        return diagnosis;
    }

    public void setDiagnosis(Diagnosis diagnosis){
        this.diagnosis = diagnosis;
    }

    public DiagnosisState getDiagnosisState() {
        return diagnosisState;
    }

    public void setDiagnosisState(DiagnosisState diagnosisState) {
        this.diagnosisState = diagnosisState;
    }

    public Reception getReception(){
        return reception;
    }

    public void setReception(Reception reception){
        this.reception = reception;
    }

    public HealthProfessional getHealthProfessional(){
        return healthProfessional;
    }

    public void setHealthProfessional(HealthProfessional healthProfessional){
        this.healthProfessional = healthProfessional;
    }

}

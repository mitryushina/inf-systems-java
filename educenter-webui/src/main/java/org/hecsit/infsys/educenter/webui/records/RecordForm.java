package org.hecsit.infsys.educenter.webui.records;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class RecordForm {

    @NotEmpty
    @Size(max = 1024, message = "The text should not be longer than 1024 characters!")
    private String writing;

    @NotEmpty
    @Size(max = 255, message = "The text should not be longer than 255 characters!")
    private String diagnosisName;

    @NotEmpty
    @Size(max = 1024, message = "The text should not be longer than 1024 characters!")
    private String diagnosisDescription;

    @NotEmpty
    private String diagnosisTypeName;


    public String getWriting() {
        return writing;
    }

    public void setWriting(String writing) {
        this.writing = writing;
    }

    public String getDiagnosisName() {
        return diagnosisName;
    }

    public void setDiagnosisName(String diagnosisName) {
        this.diagnosisName = diagnosisName;
    }

    public String getDiagnosisDescription() {
        return diagnosisDescription;
    }

    public void setDiagnosisDescription(String diagnosisDescription) { this.diagnosisDescription = diagnosisDescription; }

    public String getDiagnosisTypeName() {
        return diagnosisTypeName;
    }

    public void setDiagnosisTypeName(String diagnosisTypeName) {
        this.diagnosisTypeName = diagnosisTypeName;
    }

}

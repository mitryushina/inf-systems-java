package org.hecsit.infsys.educenter.webui.records;

import org.hecsit.infsys.educenter.core.diagnoses.DiagnosesApi;
import org.hecsit.infsys.educenter.core.healthProfessionals.HealthProfessionalsApi;
import org.hecsit.infsys.educenter.core.patients.AdditionalInfo;
import org.hecsit.infsys.educenter.core.patients.PatientsApi;
import org.hecsit.infsys.educenter.core.receptions.Reception;
import org.hecsit.infsys.educenter.core.receptions.ReceptionsApi;
import org.hecsit.infsys.educenter.core.records.Record;
import org.hecsit.infsys.educenter.core.records.RecordsApi;
import org.hecsit.infsys.educenter.core.services.Service;
import org.hecsit.infsys.educenter.core.services.ServicesApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;

@Controller
@RequestMapping("records")
public class RecordController {
    private final RecordsApi recordsApi;
    private final ReceptionsApi receptionsApi;
    private final PatientsApi patientsApi;
    private final DiagnosesApi diagnosesApi;
    private final HealthProfessionalsApi healthProfessionalsApi;
    private final ServicesApi servicesApi;

    @Autowired
    public RecordController(RecordsApi recordsApi, ReceptionsApi receptionsApi, PatientsApi patientsApi,
                            DiagnosesApi diagnosesApi, HealthProfessionalsApi healthProfessionalsApi,
                            ServicesApi servicesApi) {
        this.recordsApi = recordsApi;
        this.receptionsApi = receptionsApi;
        this.patientsApi = patientsApi;
        this.diagnosesApi = diagnosesApi;
        this.healthProfessionalsApi = healthProfessionalsApi;
        this.servicesApi = servicesApi;
    }

    //TODO: todo todo todo todooo
    @RequestMapping(value = "/{receptionId}", method = RequestMethod.GET)
    public String create(@PathVariable Long receptionId, ModelMap model) {

        Reception reception = receptionsApi.getReception(receptionId);
        model.addAttribute(reception);

        List<AdditionalInfo> additionalInfos = patientsApi.getAdditionalInfos(receptionsApi.getPatientId(receptionId));
        model.addAttribute(additionalInfos);

        List<Service> services = servicesApi.getServices();
        model.addAttribute(services);

        RecordForm form = new RecordForm();
        model.addAttribute("form", form);

        return "records/record";
    }

    @RequestMapping(value = "/{receptionId}",method = RequestMethod.POST)
    public String create( @Valid @ModelAttribute("form") RecordForm form, @PathVariable Long receptionId,
                         BindingResult bindingResult, final RedirectAttributes redirectAttributes) throws IOException, TimeoutException {
       if (bindingResult.hasErrors()) {
            return "records/record";
        }

        Long recordId = recordsApi.createRecord(form.getWriting(),
                patientsApi.getPatientById(receptionsApi.getPatientId(receptionId)),
                diagnosesApi.createDiagnosis(form.getDiagnosisName(),
                        form.getDiagnosisDescription(),
                        form.getDiagnosisTypeName(),
                        patientsApi.getPatientById(receptionsApi.getPatientId(receptionId))),
                receptionsApi.getReception(receptionId),
                healthProfessionalsApi.getHealthProfessionalById(Long.parseLong("3")));

        receptionsApi.changeType(receptionId);

        recordsApi.sendMessage(recordId);

       // JOptionPane.showMessageDialog(null,"Record was created");

        return "redirect:/receptions";
    }


    @RequestMapping(value = "/history{patientId}", method = RequestMethod.GET)
    public String showHistory(@PathVariable Long patientId, ModelMap model) {
        List<Record> records = recordsApi.getRecordsByPatientId(patientId);
        model.addAttribute(records);
        return "records/history";
    }

    @RequestMapping(value = "/{recordId}readonly", method = RequestMethod.GET)
    public String showReadonlyRecord(@PathVariable Long recordId, ModelMap model) {
        Record record = recordsApi.getRecordById(recordId);
        model.addAttribute(record);
        return "records/readonly";
    }


/*
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String getRecord(@PathVariable Long id, ModelMap model) {
        Record record = recordsApi.getRecord(id);
        model.addAttribute("form", record);
        return "records/record";
    }

    */

  /*  @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String create(@Valid @ModelAttribute("form") CourseForm form,
                         BindingResult result, final RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            return "courses/create";
        }
        recordsApi.createRecord(form.getCode(), form.getTitle(), form.getComplexityId());
        redirectAttributes.addFlashAttribute(
                "message", MessageFormat.format("Course {0} was added.", form.getCode()));
        return "redirect:/courses";
    }

    @ModelAttribute("complexityList")
    public List<CourseComplexity> addComplexities() {
        return coursesApi.getCourseComplexities();
    }*/
}


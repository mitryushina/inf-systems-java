package org.hecsit.infsys.educenter.webui.receptions;

import org.hecsit.infsys.educenter.core.receptions.Reception;
import org.hecsit.infsys.educenter.core.receptions.ReceptionsApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping("receptions")
public class ReceptionController {
    private final ReceptionsApi receptionsApi;

    @Autowired
    public ReceptionController(ReceptionsApi receptionsApi) {
        this.receptionsApi = receptionsApi;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String list(ModelMap model) {
        List<Reception> receptions = receptionsApi.getReceptions();
        model.addAttribute(receptions);
        return "dashboard";
    }
}


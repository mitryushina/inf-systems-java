package org.hecsit.infsys.educenter.webui.courses;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CourseForm {

    @NotEmpty
    @Size(max = 255)
    private String code;

    @NotEmpty
    @Size(max = 1024)
    private String title;

    @NotNull
    private Long complexityId;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getComplexityId() {
        return complexityId;
    }

    public void setComplexityId(Long complexityId) {
        this.complexityId = complexityId;
    }
}

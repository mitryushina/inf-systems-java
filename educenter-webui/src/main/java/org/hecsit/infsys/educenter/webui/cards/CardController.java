package org.hecsit.infsys.educenter.webui.cards;

import org.hecsit.infsys.educenter.core.patients.Patient;
import org.hecsit.infsys.educenter.core.patients.PatientsApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by Наталья on 12.04.2016.
 */
@Controller
@RequestMapping("cards")
public class CardController {
    private final PatientsApi patientsApi;

    @Autowired
    public CardController(PatientsApi patientsApi) {
        this.patientsApi = patientsApi;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String list(ModelMap model) {
        List<Patient> patients = patientsApi.getPatients();
        model.addAttribute(patients);
        return "dashboard";
    }
}

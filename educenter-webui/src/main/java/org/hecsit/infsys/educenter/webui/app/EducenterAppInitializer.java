package org.hecsit.infsys.educenter.webui.app;

import org.hecsit.infsys.educenter.core.CoreSpringConfig;
import org.sitemesh.config.ConfigurableSiteMeshFilter;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.*;
import java.util.EnumSet;

public class EducenterAppInitializer implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext container) throws ServletException {
        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
        rootContext.register(CoreSpringConfig.class);
        container.addListener(new ContextLoaderListener(rootContext));

        AnnotationConfigWebApplicationContext servletContext = new AnnotationConfigWebApplicationContext();
        servletContext.register(WebAppSpringConfig.class);

        ServletRegistration.Dynamic registration =
                container.addServlet("dispatcher", new DispatcherServlet(servletContext));
        registration.setLoadOnStartup(1);
        registration.addMapping("/");

        FilterRegistration.Dynamic sitemeshRegistration = container.addFilter(
                "sitemesh", new ConfigurableSiteMeshFilter());
        sitemeshRegistration.addMappingForServletNames(EnumSet.of(DispatcherType.REQUEST), true, "dispatcher");
    }
}

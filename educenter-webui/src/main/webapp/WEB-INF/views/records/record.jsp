<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<s:url value="/records/${reception.getId()}" var="addRecordUrl" />
<html>
<head>
    <title>New Record | MedCenter</title>

    <link type="text/css" rel="stylesheet" href="../../assets/css/stylesheet.css"/>
    <script src="../../assets/js/record.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <header class="page-header">
                <h2>MedCenter <small> | health cards management system</small></h2>
            </header>
        </div>
    </div>

    <div class="row" >
        <div class="col-lg-9">
            <label for="patientField">Patient:</label><input id="patientField" type="text" readonly value= "${reception.getPatient().getFullName()}"/>
            <label for="timeField">Time:</label><input id="timeField" type="text" readonly value= "${reception.getDate()}"/>
        </div>
        <div class="col-lg-3">
            <button class="btn bnt-default" id="otherRecords" name="history${reception.getPatient().getId()}">
                Show Patient's Records <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div id="additionalInfo">
                <table class="table table-striped" id="additionalInfoTable">
                    <thead>
                    <tr>
                        <th>Type</th>
                        <th>Info</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:if test="${empty additionalInfoList}">
                        <tr>
                            <td colspan="3">No Additional Info</td>
                        </tr>
                    </c:if>
                    <c:if test="${not empty additionalInfoList}">
                        <c:forEach var="item" items="${additionalInfoList}">
                            <tr >
                                <td>${item.getInfoType()}</td>
                                <td>${item.getInfo()}</td>
                            </tr>
                        </c:forEach>
                    </c:if>
                    </tbody>
                </table>
            </div>
            <button class="btn bnt-default glyphicon glyphicon-menu-down" id="hiddenButton">See Additional Info</button>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
                <hr id="hrRecord" width="100%">
        </div>
        <div class="col-lg-6"></div>
        <button class="btn bnt-default col-offset-3">Attach document</button>
        <%--<form method="POST" action="uploadFile" enctype="multipart/form-data">
            File to upload: <input type="file" name="file"><br />
            Name: <input type="text" name="name"><br /> <br />
            <input type="submit" value="Upload"> Press here to upload the file!
        </form>--%>
        <form:form method="post" modelAttribute="form" action="${addRecordUrl}">
            <div class="col-lg-6">
                <div class="form-group">
                    <label path="writing">Description:</label>
                    <form:textarea path="writing" rows="6" cols="100"></form:textarea>
                    <form:errors path="writing" cssClass="error" id="descriptionField"/>
                </div>
                <div class="col-lg-12">
                    <hr width="100%" color="white">
                </div>
                <div class="form-group">
                    <label path="diagnosisName">Diagnosis:</label>
                    <form:input path="diagnosisName" type="text" />
                    <form:errors path="diagnosisName" cssClass="error"/>
                </div>
                <div class="form-group">
                    <label path="diagnosisTypeName">Type:</label>
                    <p><form:radiobutton path="diagnosisTypeName" value="NOT_FINAL" />NotFinal
                        <form:radiobutton path="diagnosisTypeName" value="FINAL" />Final</p>
                </div>
                <div class="form-group">
                    <label path="diagnosisDescription">Diagnosis Description:</label>
                    <form:textarea path="diagnosisDescription" rows="4" cols="100"></form:textarea>
                    <form:errors path="diagnosisDescription" cssClass="error" id="descriptionField"/>
                </div>
                <div class="col-lg-12">
                    <hr width="100%" color="white">
                </div>
            </div>


            <div class="col-lg-12">
                <label>Services:</label>
                <div id="service">
                    <table class="table table-bordered scrolling" id="serviceTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Type</th>
                            <th>Name</th>
                            <th>Amount</th>
                        </tr>
                        </thead>

                        <tbody>
                        <c:if test="${empty serviceList}">
                            <tr>
                                <td colspan="3">No Additional Info</td>
                            </tr>
                        </c:if>
                        <c:if test="${not empty serviceList}">
                            <c:forEach var="item" items="${serviceList}">
                                <tr >
                                    <td></td>
                                    <td>${item.getServiceType()}</td>
                                    <td>${item.getName()}</td>
                                    <td><input type="number" min="0" max="100" step="1"/></td>
                                </tr>
                            </c:forEach>
                        </c:if>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-lg-12">
                <button class="btn bnt-default" id="createRecordButton" type="submit">Create Record</button>
            </div>
        </form:form>
    </div>
</div>
</body>
</html>

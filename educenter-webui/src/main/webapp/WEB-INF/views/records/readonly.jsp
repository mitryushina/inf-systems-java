<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<s:url value="readonly" var="ReadonlyHistoryUrl" />
<html>
<head>
    <title>Readonly | MedCenter</title>

    <link type="text/css" rel="stylesheet" href="../../assets/css/stylesheet.css"/>
    <script src="../../assets/js/record.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <header class="page-header">
                <h2>MedCenter <small> | health cards management system</small></h2>
            </header>
        </div>
    </div>
    <div class="row" >
        <div class="col-lg-9">
            <label for="patientField">Patient:</label><input id="patientField" type="text" readonly value= "${record.getPatient().getFullName()}"/>
            <label for="timeField">Time:</label><input id="timeField" type="text" readonly value= "${record.getReception().getDate()}"/>
            <label for="healthProfessionalField">Health Professional:</label><input id="healthProfessionalField" type="text" readonly value= "${record.getHealthProfessional().getFullName()}"/>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <hr width="100%">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
             <label class="col-lg-1" for="writing">Writing:</label>
             <textarea class="col-lg-6" id="writing" rows="6" cols="100" readonly>${record.getWriting()}</textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <label class="col-lg-3" for="diagnosis">Diagnosis:</label>
            <input class="col-lg-9" id="diagnosis" type="text" size="100" readonly value="${record.getDiagnosis().getName()} ( ${record.getDiagnosis().getType()} )"/>
        </div>

        <div class="col-md-7">
            <label class="col-lg-3" for="diagnosisDescription">Diagnosis Description:</label>
            <textarea class="col-lg-9" id="diagnosisDescription" rows="4" cols="100" readonly>${record.getDiagnosis().getDescription()}</textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <label class="col-lg-3">Attached documents:</label>
        </div>
    </div>




</div>
</body>
</html>

</body>
</html>

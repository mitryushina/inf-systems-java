<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<html>
<head>
    <title><sitemesh:write property='title'/></title>
    <sitemesh:write property='head'/>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/main.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/stylesheet.css">
    <script src="${pageContext.request.contextPath}/assets/js/dashboard.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/logon.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/record.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/history.js"></script>
</head>

<!-- <s:url value="/courses" var="coursesListUrl" /> -->

<body>
<!--    <h1>EDUCEN</h1> -->
<!--    [<a href="${coursesListUrl}">Courses List</a>|Complexities List] -->
   <sitemesh:write property='body'/>
</body>
</html>
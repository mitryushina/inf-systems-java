/**
 * Created by Наталья on 25.04.2016.
 */
$(document).ready(function(){
    $('.table tbody tr').each(function (i) {
        var number = i + 1;
        $(this).find('td:first').text(number + ".");
    });

    $('#patientHistoryTable').find('tr').click(function() {
        var href = $(this).find("a").attr("href");
        if(href) {
            window.location = href;
        }
    });
});
$(document).ready(function() {
    $('.table tbody tr').each(function (i) {
        var number = i + 1;
        $(this).find('td:first').text(number + ".");
    });

    $('#patientsTable').find('tr').click(function() {
        var href = $(this).find("a").attr("href");
        if(href) {
            window.location = href;
        }
    });
    /*
    $("table tr").click(
        function(){
            alert("Hi!");
            window.location.href = "receptions";
        }); */
});

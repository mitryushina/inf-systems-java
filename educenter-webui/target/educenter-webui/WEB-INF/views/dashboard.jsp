<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<s:url value="/records/card" var="RecordUrl" />
<!DOCTYPE HTML>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link type="text/css" rel="stylesheet" href="../../assets/css/stylesheet.css"/>
    <script src="../../assets/js/dashboard.js"></script>
    <title>Patients | MedCenter</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <header class="page-header">
                <h2>MedCenter <small> | health cards management system</small></h2>
            </header>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-7">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button">
                        <span class="glyphicon glyphicon-search" aria-hidden="true"></span> Search
                    </button>
                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7" role="group" id="sort">
            <div class="radio">
                <label><input type="radio" name="optradio" checked="checked">Sorting by date</label>
            </div>
            <div class="radio">
                <label><input type="radio" name="optradio" >Sorting by name</label>
            </div>
        </div>
    </div>

    <div class="row">
        <table class="table table-striped table-hover" id="patientsTable">
            <thead>
            <tr>
                <th>#</th>
                <th>Patient</th>
                <th>Date</th>
            </tr>
            </thead>
            <tbody>
                <c:if test="${empty receptionList}">
            <tr>
                <td colspan="3">No Receptions</td>
            </tr>
            </c:if>
            <c:if test="${not empty receptionList}">
                <c:forEach var="item" items="${receptionList}">
                    <c:if test="${item.getReceptionType().toString() == 'Open'}">
                        <tr >
                            <td></td>
                            <td><a href="records/${item.getId()}">${item.getPatient().getFullName()}</a></td>
                            <td>${item.getDate()}</td>
                        </tr>
                    </c:if>
                </c:forEach>
            </c:if>
            </tbody>
        </table>
    </div>

    <div class="row">
        <nav class="col-md-12">
            <ul class="pagination">
                <li>
                    <a aria-label="Previous" onclick="getPreviousPage()">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <li class="active" onclick="getNthPage()"><a>1</a></li>
                <li onclick="getNthPage()"><a>2</a></li>
                <li onclick="getNthPage()"><a>3</a></li>
                <li onclick="getNthPage()"><a>4</a></li>
                <li onclick="getNthPage()"><a>5</a></li>
                <li onclick="getNthPage()">
                    <a aria-label="Next" onclick="getNextPage()">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>

    </div>

</div>
</body>
</html>
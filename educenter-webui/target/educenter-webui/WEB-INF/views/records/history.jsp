<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<s:url value="history" var="RecordHistoryUrl" />
<html>
<head>
    <title>History | MedCenter</title>

    <link type="text/css" rel="stylesheet" href="../../assets/css/stylesheet.css"/>
    <script src="../../assets/js/history.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <header class="page-header">
                <h2>MedCenter <small> | health cards management system</small></h2>
            </header>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div id="patientHistory">
                <table class="table table-striped table-hover" id="patientHistoryTable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Date</th>
                        <th>Diagnosis</th>
                        <th>Health Professional</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:if test="${empty recordList}">
                        <tr>
                            <td colspan="4">No Records</td>
                        </tr>
                    </c:if>
                    <c:if test="${not empty recordList}">
                        <c:forEach var="item" items="${recordList}">
                            <tr >
                                <td></td>
                                <td><a href="${item.getId()}readonly">${item.getReception().getDate()}</a></td>
                                <td>${item.getDiagnosis().getName()}</td>
                                <td>${item.getHealthProfessional().getFullName()}: <em>${item.getHealthProfessional().getPost()}</em></td>
                            </tr>
                        </c:forEach>
                    </c:if>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
</body>
</html>
